const usersData = require('./userData');

//Q1 Find all users who are interested in playing video games.
function interestedInGame(usersData, game) {

    let result = Object.entries(usersData).map((user) => {
        return user[1]
    }).filter((userDetails) => {
        game = game.toLowerCase();
        let interestsData = "";
        if (userDetails.interests != undefined) {
            interestsData = (userDetails.interests.toString()).toLowerCase();
        } else {
            interestsData = (userDetails.interest.toString()).toLowerCase();
        }
        return interestsData.includes(game);
    })

    return result;

}
console.log(interestedInGame(usersData, "Video Games"));


//2. Find all users staying in Germany.
function searchBasedOnNationality(usersData, contry) {
    let result = Object.entries(usersData).map((user) => {
        return user[1];
    }).filter((userDetails) => {
        return userDetails.nationality == contry;
    })

    return result;
}


//Q4 Find all users with masters Degree.
function seachBasedOnQualification(usersData, degree) {
    let result = Object.entries(usersData).map((user) => {
        return user[1];
    }).filter((userDetails) => {
        return userDetails.qualification == degree;
    })

    return result;
}
